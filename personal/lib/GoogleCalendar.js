const { google } = require('googleapis')

module.exports = class GoogleCalendar {
  constructor() {
    const { OAuth2 } = google.auth
    const oAuth2Client = new OAuth2(process.env.CLIENT_ID, process.env.CLIENT_SECRET)
    // Call the setCredentials method on our oAuth2Client instance and set our refresh token.
    oAuth2Client.setCredentials({
      refresh_token: process.env.REFRESH_TOKEN
    })
    // Create a new calender instance.
    this.calendar = google.calendar({ version: 'v3', auth: oAuth2Client })
    this.event = {}
  }

  createEvent(payload) {
    const { summary, description, colorId = 6, eventStartTime, eventEndTime, attendees, reminders } = payload
    this.event = {
      summary: summary,
      description: description,
      colorId: colorId,
      start: {
        dateTime: eventStartTime
      },
      end: {
        dateTime: eventEndTime
      },
      attendees: attendees || [],
      reminders
    }
  }

  async addToCalendar() {
    const { summary, start } = this.event
    if (!summary || !start) {
      console.log('Missing Summary or start')
      return null
    }
    const res = await this.calendar.events.insert({ calendarId: 'primary', resource: this.event })
    if (res.data) return res.data
    return null

  }

  async isBusy() {
    const { start, end } = this.event
    if (!start || !end) return null
    const checker = await this.calendar.freebusy.query({
      resource: {
        timeMin: start.dateTime,
        timeMax: end.dateTime,
        items: [{ id: 'primary' }]
      }
    })

    if (checker.data) {
      if (checker.data.calendars.primary.busy.length > 0) return true
    }
    return false
  }

  async getAllEvent() {
    const { start, end } = this.event
    const res = await this.calendar.events.list({
      calendarId: 'primary',
      timeMin: start.dateTime,
      maxResults: 10,
      singleEvents: true,
      orderBy: 'startTime'
    })
    if (!res.data) return null
    return res.data
  }

 

  async getColor() {
    const res = await this.calendar.colors.get()
    if (res.data)
      return res.data
    return null
  }

  async calendarList() {
    const res = await this.calendar.calendarList.list()
    if (res.data)
      return res.data
    return null
  }
  // Need CalendarId and EventId
  // 1. Get CalendarList() // find your calendarId, (Personal Calendar, id is always including email) - requeired
  // 2. Get getAllEvent() // find your eventId - required

 // alwaysIncludeEmail : boolean- optional -> include email infor
  // maxAttendees - int - optional -> Set max number of attendees -> if null will get default
  // timeZone - string - optional -> Set timezone of data resonse -> if null will get default GMT
  async getDetailEvent(calendarId, eventId) {
    const params = { calendarId, eventId }
    if (sendNotifications && sendUpdates)
      params = { ...params, sendNotifications, sendUpdates }
    const res = await this.calendar.events.get(params)
    if (res.data)
      return res.data
    return null
  }


  // sendNotifications: boolean // optional
  // sendUpdates: string // optional - [all, externalOnly, none] - "externalOnly" - Notifications are sent to non-Google Calendar guests only.
  async deleteEventFromCalendar(calendarId, eventId, sendNotifications, sendUpdates) {
    const params = { calendarId, eventId }
    if (sendNotifications && sendUpdates)
      params = { ...params, sendNotifications, sendUpdates }
    const res = await this.calendar.events.delete(params)
    if (res.data)
      return res.data
    return null
  }

}
