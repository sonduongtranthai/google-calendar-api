require('dotenv').config()
const GoogleCalendar = require('./lib/GoogleCalendar')

const GGCalendar = new GoogleCalendar()
// Create a new event start date instance for temp uses in our calendar.
const eventStartTime = new Date()

eventStartTime.setDate(eventStartTime.getDate() + 1)

// Create a new event end date instance for temp uses in our calendar.
const eventEndTime = new Date()
eventEndTime.setDate(eventEndTime.getDate() + 1)
eventEndTime.setMinutes(eventEndTime.getMinutes() + 60) // set deadline

const summary = 'Ahihi Summary'
const description = 'Ahihi description'
// const attendees = [example@gmail.com] Email to share here
const attendees = []
const reminders = {
    useDefault: false,
    overrides: [
        { method: 'email', minutes: 24 * 60 }, // Reminder User by email before 24h
        { method: 'popup', minutes: 10 },
    ],
}
// Will reminder by email or calendarApp
// Init event 
GGCalendar.getColor() // Get all color to add  colorId: 6
GGCalendar.createEvent({ summary, description, colorId: 6, eventStartTime, eventEndTime, attendees, reminders })
GGCalendar.isBusy()
GGCalendar.addToCalendar()
GGCalendar.getAllEvent().then(r => {
    console.log('r.items', r.items)
})
GGCalendar.calendarList().then(r => {
    console.log('r', r)
})
